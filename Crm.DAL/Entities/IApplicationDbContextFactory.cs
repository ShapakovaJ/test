﻿namespace Crm.DAL.Entities
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
