﻿using Microsoft.AspNetCore.Identity;

namespace Crm.DAL.Entities
{
    public class Role : IdentityRole<string>, IEntity
    {
    }
}
