﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Crm.DAL.Entities
{
    public class User : IdentityUser<string>, IEntity
    {
    }
}
