﻿using System;
using Crm.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Crm.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
