﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Crm.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
